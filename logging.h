/*
 * (c) 2020 - José A. García Sánchez
 */
#ifndef JAGLOG4C_LOGGING_H
#define JAGLOG4C_LOGGING_H

/**
 * @defgroup logging Logging
 * @brief Logging functionality to write messages to standard output.
 * @{
 */

/**
 * @brief Writes a formatted message to output.
 *
 * @param[in] string Formatted message to be written
 * @param ... Addtional values to be put in the formatted message
 */
void logging_log(const char *string, ...);

/**
 * @}
 */
#endif //JAGLOG4C_LOGGING_H
